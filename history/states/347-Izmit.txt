
state={
	id=347
	name="STATE_347"
	manpower = 1
	resources={
		chromium=19 # was: 26
		steel = 9
	}
	
	state_category = rural

	history={
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			904 = {
				naval_base = 2
			}
		}
	}

	provinces={
		890 904 3905 3919 6871 11811 11866 11883 
	}
}
