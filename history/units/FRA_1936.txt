﻿##### Division Templates #####
division_template = {
	name = "Division d'Infanterie"		
	division_names_group = FRA_INF_01
	# Represents: Division d'Infanterie (Series A and B), Div. d'Inf. de Forteresse
	# Difference is their equipment
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 } 	# Heavy Arty Regiment had 2x 155mm battalions
		engineer = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Division Coloniale"		
	division_names_group = FRA_COL_01
	# Represents: Div. d'Inf. Coloniale, Div. d'Inf. Nord-Africaine
	# Difference from DI is their equipment
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 } 	# Heavy Arty Regiment had 2x 155mm battalions
	}
}
division_template = {
	name = "Division d'Infanterie Motorisée"	# Division d'Infanterie Motorisée
	division_names_group = FRA_MOT_01

	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }
		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
		motorized = { x = 2 y = 2 }
	}
	support = {
		mot_recon = { x = 0 y = 0 }      # Recon Group consisted of 26 ACs + motorcycles
		artillery = { x = 0 y = 1 }  # Heavy Arty Regiment had 1x 155mm, 1x 105mm battalions
		engineer = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Division d'Infanterie Alpine"	# Division d'Infanterie Alpine
	division_names_group = FRA_MNT_01
	# Note: trimmed to 3x Rgts w/ arty bn added, 1939

	regiments = {
		mountaineers = { x = 0 y = 0 }	# Regular mountain infantry (Infanterie Alpine)
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }	
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
		mountaineers = { x = 2 y = 0 }	# Chasseurs Alpins (HOI3 elites)
		mountaineers = { x = 2 y = 1 }	# Chasseurs Alpins (HOI3 elites)
		mountaineers = { x = 2 y = 2 }	# Chasseurs Alpins (HOI3 elites)
		mountaineers = { x = 3 y = 0 }	# Chasseurs Alpins (HOI3 elites)
		mountaineers = { x = 3 y = 1 }	# Chasseurs Alpins (HOI3 elites)
		mountaineers = { x = 3 y = 2 }	# Chasseurs Alpins (HOI3 elites)
	}
}
division_template = {
	name = "Division de Cavalerie" 	# Division de Cavalerie
	division_names_group = FRA_CAV_01
	# Note: Transformed to DLC, DLM divisions, 1936-39 
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
	support = {
		recon = { x = 0 y = 0 }      # Recon Group consisted of 64 ACs/scout cars
	}
}
division_template = {
	name = "Division Légère Mécanique" 		# Division Légère Mécanique
	division_names_group = FRA_MEC_01

	regiments = {
		light_armor = { x = 0 y = 0 }		# Bn. of Hotchkiss, then Souma (Med.) tanks
		light_armor = { x = 0 y = 1 }		# Bn. of Hotchkiss tanks
		motorized = { x = 1 y = 0 }			# Brigade of 2x Rgts., 2 Bns. each (later 1 Rgt. of 3x Bns.)
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }
		motorized = { x = 1 y = 3 }
	}
	support = {
		mot_recon = { x = 0 y = 0 }      # Recon Group consisted of 42 ACs + motorcycles
		engineer = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Brigade de Chars de Combat" 	# Brigade de Chars de Combat, independent armor brigades of 2x Rgts., 2 Bns. each
	division_names_group = FRA_ARM_01
	regiments = {
		light_armor = { x = 0 y = 0 }
		light_armor = { x = 0 y = 1 }
		light_armor = { x = 1 y = 0 }
		light_armor = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Brigade Coloniale"			# Represents: 2xRgt colonial forces and , usually with old equipment
	division_names_group = FRA_COL_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
	priority = 0
}

division_template = {
	name = "MP"

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 0 y = 3 }
		cavalry = { x = 0 y = 4 }

		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
		cavalry = { x = 1 y = 3 }
		cavalry = { x = 1 y = 4 }

		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
		cavalry = { x = 2 y = 2 }
		cavalry = { x = 2 y = 3 }
		cavalry = { x = 2 y = 4 }

		cavalry = { x = 3 y = 0 }
		cavalry = { x = 3 y = 1 }
		cavalry = { x = 3 y = 2 }
		cavalry = { x = 3 y = 3 }
		cavalry = { x = 3 y = 4 }

		cavalry = { x = 4 y = 0 }
		cavalry = { x = 4 y = 1 }
		cavalry = { x = 4 y = 2 }
		cavalry = { x = 4 y = 3 }
		cavalry = { x = 4 y = 4 }

	}
	support = {

	}
}

division_template = {
	name = "50W"

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 0 y = 4 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 1 y = 3 }
		infantry = { x = 1 y = 4 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		infantry = { x = 2 y = 3 }
		infantry = { x = 2 y = 4 }

		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
		infantry = { x = 3 y = 2 }
		infantry = { x = 3 y = 3 }
		infantry = { x = 3 y = 4 }

		infantry = { x = 4 y = 0 }
		infantry = { x = 4 y = 1 }
		infantry = { x = 4 y = 2 }
		infantry = { x = 4 y = 3 }
		infantry = { x = 4 y = 4 }

	}
	support = {

	}
}

##### OOB #####
units = {
	division= {
		location = 11506
		division_template = "50W"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
}

### Air Wings
air_wings = {
	# Z.d'Op. Aériennes Nord -- Lille
	29 = { 
		# Groupement de Chasse 21 
		# Groupement de Chasse 23
		fighter_equipment_0 = {
			owner = "FRA" 
			amount = 192
		}
		# Groupement de Bombardment 6
		# Groupement de Bombardment 9
		tac_bomber_equipment_1 =  {
			owner = "FRA" 
			amount = 60
		}
	}

	# Z.d'Op. Aériennes Est -- Reims
	18 = {
		# Groupement de Chasse 22
		fighter_equipment_0 = {
			owner = "FRA" 
			amount = 96
		}
	}

	# Z.d'Op. Aériennes du Alps -- Marseille
	21 = {
		# Groupement de Chasse d'Alps
		fighter_equipment_0 = {
			owner = "FRA" 
			amount = 96
		}
		# Groupement de Bombardment 1
		# Groupement de Bombardment 6
		# Groupement de Bombardment 9
		tac_bomber_equipment_1 =  {
			owner = "FRA" 
			amount = 90
		}
		# Groupement de Bombardment d'Assault 19
		tac_bomber_equipment_0 =  {
			owner = "FRA" 
			amount = 24
		}
	}

	# Z.d'Op. Aériennes Africain -- Tunis
	458 = {
		# Groupement de Chasse du Nord-Africain
		fighter_equipment_0 = {
			owner = "FRA" 
			amount = 48
		}
		# Groupement de Bombardment du Nord-Africain
		tac_bomber_equipment_1 =  {
			owner = "FRA" 
			amount = 30
		}
	}

	# Aeronautique Navale -- Cherbourg
	15 = {
		# Flotille F1
		nav_bomber_equipment_1 = {
			owner = "FRA" 
			amount = 24
		}
	}
}

#########################
## STARTING PRODUCTION ##
#########################

instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "FRA"
		}
		requested_factories = 2
		progress = 0.1
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "FRA" 
		}
		requested_factories = 1
		progress = 0.3
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = artillery_equipment_1
			creator = "FRA" 
		}
		requested_factories = 1
		progress = 0.3
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = light_tank_equipment_2
			creator = "FRA" 
		}
		requested_factories = 1
		progress = 0.4
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = motorized_equipment_1
			creator = "FRA" 
		}
		requested_factories = 1
		progress = 0.4
		efficiency = 50
	}
}
###################
